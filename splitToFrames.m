function output = splitToFrames(filename)

readerobj = VideoReader(filename);
vidFrames = read(readerobj);
numFrames = get(readerobj, 'numberOfFrames');
parfor k = 1 : numFrames
mov(k).cdata = vidFrames(:,:,:,k);
%mov(k).colormap = [];
end

output = mov;

end