function [LQImages,HQImages] = makeImageStructs()
    lqdir = 'C:\Users\Todd Enfield\Documents\University\Vision\Vision Major Project\LQImages\';
    LQImages(1) = struct('cdata',imread( strcat(lqdir,'00001LQ.png')));
    LQImages(2) = struct('cdata',imread( strcat(lqdir,'00041LQ.png')));
    LQImages(3) = struct('cdata',imread( strcat(lqdir,'00281LQ.png')));
    LQImages(4) = struct('cdata',imread( strcat(lqdir,'00401LQ.png')));
    LQImages(5) = struct('cdata',imread( strcat(lqdir,'00441LQ.png')));
    LQImages(6) = struct('cdata',imread( strcat(lqdir,'00721LQ.png')));
    LQImages(7) = struct('cdata',imread( strcat(lqdir,'00921LQ.png')));
    LQImages(8) = struct('cdata',imread( strcat(lqdir,'00961LQ.png')));
    LQImages(9) = struct('cdata',imread( strcat(lqdir,'01321LQ.png')));
    LQImages(10) = struct('cdata',imread( strcat(lqdir,'01961LQ.png')));
    LQImages(11) = struct('cdata',imread( strcat(lqdir,'02441LQ.png')));
    
    hqdir = 'C:\Users\Todd Enfield\Documents\University\Vision\Vision Major Project\HQImages\';
    HQImages(1) = struct('cdata',imread( strcat(hqdir,'00001HQ.png')));
    HQImages(2) = struct('cdata',imread( strcat(hqdir,'00041HQ.png')));
    HQImages(3) = struct('cdata',imread( strcat(hqdir,'00281HQ.png')));
    HQImages(4) = struct('cdata',imread( strcat(hqdir,'00401HQ.png')));
    HQImages(5) = struct('cdata',imread( strcat(hqdir,'00441HQ.png')));
    HQImages(6) = struct('cdata',imread( strcat(hqdir,'00721HQ.png')));
    HQImages(7) = struct('cdata',imread( strcat(hqdir,'00921HQ.png')));
    HQImages(8) = struct('cdata',imread( strcat(hqdir,'01961HQ.png')));
    HQImages(9) = struct('cdata',imread( strcat(hqdir,'02441HQ.png')));
    HQImages(10) = struct('cdata',imread( strcat(hqdir,'01501HQ.png')));


    
end