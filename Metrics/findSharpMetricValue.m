function currentchoice = findSharpMetricValue(LowQuality,HighQuality)
    count = 0;
    countThresh = 100;
    threshold = 10;
    metric = sharpMetric(HighQuality);
    currentmetric = 1000;
    currentchoice = 0;
    choice1 = 0.5;
    choice2 = 0.2;
    metric1 = 1000;
    metric2 = 1000;
    while(1)
        check = abs(metric - currentmetric);
        count = count+1
        if(check < threshold || count>=countThresh)
           break; 
        end
        
        metric1 = sharpMetric(sharpen(LowQuality,choice1,0));
        metric2 = sharpMetric(sharpen(LowQuality,choice2,0));
        
        if( abs(metric - metric1) < abs(metric - metric2))
            choice1 = choice1;
            choice2 = (choice1+choice2)/2;
            currentchoice = choice1;
            currentmetric = metric1;
        else
            choice2 = choice2;
            choice1 = (choice1+choice2)/2;
            currentchoice = choice2;
            currentmetric = metric2;
        end
        
        
    end


end