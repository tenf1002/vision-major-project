% % % Read video
% % v = VideoReader('goodQual.mp4');
% % 
% % while hasFrame(v)
% %     video = readFrame(v);
% % end
clear;
clc;
close all;
profile on
global goodProp;
good = imread('goodQual.JPG');
[goodProp.rows goodProp.cols goodProp.color] = size(good);
badOld = imread('badQual.JPG');

for n = 1:goodProp.color
    bad(:,:,n) = imresize(badOld(:,:,n),[goodProp.rows,goodProp.cols]);
end
% bad = imgaussfilt(good);
%% Brightness
bright1 = brightnessMetric(good);
bright2 = brightnessMetric(bad);

%% Contrast
% Get contrast values - RMS
C1 = contrastMetric(good);
C2 = contrastMetric(bad);

CW1 = weberCont(good);
CW2 = weberCont(bad);

CM1 = michCont(good);
CM2 = michCont(bad);



%% Shannon Information

SI1 = shannonInfo(good);
SI2 = shannonInfo(bad);

%% Blur
disp('1');
B1 = blurMarz(good);
disp('2');
B2 = blurMarz(bad);

% Second method
B_1 = blurMetric(good);
B_2 = blurMetric(bad);

%% Sharpness
disp('1');

sharp1 = sharpMetric(good);
disp('s');
sharp2 = sharpMetric(bad);

%% Spatial information
spatI1 = spatInfo(good);
spatI2 = spatInfo(bad);

%% Spatial entropy
spatE1 = spatEntropy(good);
spatE2 = spatEntropy(bad);
p = profile('info');


%% Tabulate
Good = [C1 CW1 CM1]';
Bad = [C2 CW2 CM2]';

table(Good,Bad,'RowNames',{'RMS','Weber','Michelson'})



%Blur
Good = [B1,B_1]';
Bad = [B2,B_2]';
table(Good,Bad,'RowNames',{'Mariziliano Blur','No Reference Blur metric'})

%Sharpness


%Spatial information and entropy
Good = [bright1,sharp1,SI1,spatI1,spatE1]';
Bad = [bright2,sharp2,SI2,spatI2,spatE2]';
table(Good,Bad,'RowNames',{'Brightness','Sharpness','Shannon Information','Spatial Information','Spatial Entropy'})