function result = blurMarz(image)
    % Check if rgb
    global goodProp;
    [goodProp.rows goodProp.cols goodProp.color] = size(image);
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    % Blur image
    imBlur = imgaussfilt(image);
    % Filter
    filtIm = edge(image,'Sobel',(10/255),'vertical');
    
    % Get values where edge occurs
    [rowEdge colEdge] = find(filtIm == 1);
    % Go through all elements
    for r = 1:length(rowEdge)
        % Row value and column value
        rowVal = rowEdge(r);
        colVal = colEdge(r);
        
        % Local maxima and minima
        [~ ,minInd] = lmin(imBlur(rowVal,:));
        [~,maxInd] = lmax(imBlur(rowVal,:));

        % Get nearest min
        nearMin = minInd(knnsearch(minInd',colVal));
        nearMax = maxInd(knnsearch(maxInd',colVal));
        
        % d val
        d_val(r) = abs(nearMin-nearMax);
        

    end
    
   
    % Get result
    result = sum(d_val)/length(d_val);
end