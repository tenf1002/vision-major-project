function result = shannonInfo(image)
    % Check if rgb
    global goodProp;
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    % Convert to double
    image = im2double(image);
    
    % Get normalised histogram
    normHist = imhist(image)./numel(image);
    
    % Get values
    array = normHist.*log2(1./normHist);
    
    % Result
    result = nansum(array);
    
end