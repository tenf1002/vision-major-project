% Get contrast
function C = contrastMetric(image)
    % Check if rgb
    global goodProp;
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    % Convert to double
    image = im2double(image);
    
	% Get size
	[rows cols] = size(image);

	% Get square difference
	sqDiff = (image - mean(mean(image))).^2;
    % Get sum
    sqDiffSum = sum(sqDiff(:));
	% Get C
	C = sqrt((1/(rows*cols)) * sqDiffSum );

end

