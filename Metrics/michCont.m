function C = michCont(image)
    % Check if rgb
    global goodProp;
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    % Convert to double
    image = im2double(image);
    
	% Get size
	[rows cols] = size(image);
    
    % Numerator
    num = (max(image)-min(image));
    
    % Denominator
    den = max(image) + min(image);
    
    % Get contrast
    C = num/den;
    
end