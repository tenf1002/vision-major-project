function result = spatInfo(image)

    % Check if rgb
    global goodProp;
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    
    % Get filter
    w = fspecial('sobel');
    
    % Get filtered image
    filtIm = imfilter(image,w,'replicate');
    
    % Get output
    result = std(im2double(filtIm(:)),1);
    
end