function C = weberCont(image)
    % Check if rgb
    global goodProp;
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    % Convert to double
    image = im2double(image);
    
	% Get size
	[rows cols] = size(image);
    
    % Delta L
    deltaL = (max(image)-min(image))/2;
    
    % Get contrast
    C = deltaL/min(image);
    
end