% Get contrast
function C = contrastMetric(image)

	% Get size
	[rows cols] = size(image);

	% Get square difference
	sqDiff = (image - mean(mean(image))).^2;

	% Get C
	C = sqrt((1/(rows*cols)) * sqDiff );

end

