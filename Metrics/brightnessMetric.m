function result = brightnessMetric(image)
    % Check if rgb
    global goodProp;
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    
    result = mean(image(:));
end