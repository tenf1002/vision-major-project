function result = spatEntropy(image)
    % Check if rgb
    global goodProp;
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    
    % Get filter
    w = fspecial('sobel');
    
    % Get filtered image
    filtIm = imfilter(image,w,'replicate');
    
    % Convert to double
    image = im2double(filtIm);
    
    % Get normalised histogram
    normHist = imhist(image)./numel(image);
    
    % Get values
    array = normHist.*log2(1./normHist);
    
    % Result
    result = nansum(array);
    
end