function displaybothvideo(frames1,frames2)

figure;
for i = 1:length(frames1);
    subplot(1,2,1);
    imshow(frames1(i).cdata);
    subplot(1,2,2);
    if(i < numel(frames2))
        imshow(frames2(i).cdata);
    end
    drawnow;
end

end