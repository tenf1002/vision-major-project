function displayVideo(Frames)
figure;
for i = 1:length(Frames);
    imshow(Frames(i).cdata);
    drawnow;
end