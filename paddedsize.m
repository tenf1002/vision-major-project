%Tutorial 2 Question 2(a): Padding

function [P,Q] = paddedsize(InputImage, Option,param1)

[m,n] = size(InputImage);

switch nargin
    %No input option
    case 1
        P = 2*m-1;
        Q = 2*n-1;
    case 2
        if(strcmp(Option,'min'))
            P = 2*m-1;
            Q = 2*n-1;
        else
            P = 0;
            Q = 0;
            disp('Error: Option String');
        return;
        end
    case 3
        if(strcmp(Option,'multiplier'))
            P = param1*2*m-1;
            Q = param1*2*n-1;
        elseif(strcmp(Option,'power'))
            P = (2*m-1)^param1;
            Q = (2*n-1)^param1;
        elseif(strcmp(Option,'additional'))
            P = 2*m-1+param1;
            Q = 2*n-1+param1;
        end
    otherwise
        P = 0;
        Q = 0;
        disp('Error: wrong amount of input arguments');
        return;
end
    
    P=P+m;
    Q = Q+n;
end