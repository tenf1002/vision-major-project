%Classify HQ Images function
%Classifies the HQ images depending on the structural and colour histograms
%of a particular image

function [classedHQImages,matchedHQ,matchedLQ,differences] = ClassifyHQImages(lowQStruct,HQFrames)

    %First, begin by extracting an image of each class from the lowQStruct
    classTakenArray = [];
    element = 1;
    threshold = 0.02;
    subtractThresh = 1000;
    %classStruct = struct([]);
    %for i = 1:numel(lowQStruct)
        %string = lowQStruct(i).Class(numel(lowQStruct(i).Class));
        
        %if(~ismember(string,classTakenArray))
            
            %currentImage = lowQStruct(i).cdata;
            %classStruct(element) = struct('Class',strcat('Class',string),'cdata',currentImage);
            %element = element+1;
            %classTakenArray = [classTakenArray,string];
            
        %end
    %end
    
    %One image of each type has been extracted from the struct, and now we
    %classify the HQFrames such that the difference between each set of
    %images is determined.
    
    Differences = [];
    
    for i = 1:numel(HQFrames)
        
        for j = 1:numel(lowQStruct)
            %Find the similarity between the HQ Frames, and the LQ Frames
            [r,c] = size(lowQStruct(j).cdata);
            hqchanged = imresize(HQFrames(i).cdata, [r,c]);
            Differences(i,j) = 1 - CompareFrames(hqchanged,lowQStruct(j).cdata);
        end
        
    end
    differences = Differences;
    %Now go through and classify the images to be as close as possible
    %within a threshold.
    [rows,cols] = size(Differences);
    element = 1;
    matchel = 1;
    for i = 1:rows
        
        minMatrix = min(Differences(:));
        [row,col] = find(Differences==minMatrix);
        
        %We have found the minimum, add this to the struct and then set all
        %elements within that row to be 100;
        classedHQImages(element) = struct('Class',strcat('Class',num2str(col-1)),'cdata',HQFrames(row).cdata);
        if(Differences(row,col) < threshold)
            matchedHQ(matchel) = struct('Class',strcat('Class',num2str(col-1)),'cdata',HQFrames(row).cdata);
            matchedLQ(matchel) = struct('Class',strcat('Class',num2str(col-1)),'cdata',lowQStruct(col).cdata,'framePos',lowQStruct(col).framePos);
            matchel = matchel+1;
        end
        Differences(row,:) = 100;
        element = element +1;
        
        
    end
    
end