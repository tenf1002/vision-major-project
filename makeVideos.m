% Write video
clear;
clc;
close all;

% Get High quality video
hqVideo = VideoWriter('hqVideo.avi','Uncompressed AVI');
hqFolder = 'HQImages/';
hqImages = dir('HQImages/*.png');
open(hqVideo);
% Put into video
for n = 1:length(hqImages)
    
    % Write to video
    frameHQ = imread([hqFolder,hqImages(n).name]);
    writeVideo(hqVideo,frameHQ);
end
close(hqVideo);

% Get HQ size
[rows,cols,~] = size(frameHQ);


%% LQ
% Get low quality video
lqVideo = VideoWriter('lqVideo.avi','Uncompressed AVI');
lqFolder = 'LQImages/';
lqImages = dir('LQImages/*.png');
open(lqVideo);
% Put into video
for n = 1:length(hqImages)
    
    % Write to video
    frameLQ = imread([lqFolder,lqImages(n).name]);
    frameLQ = imresize(frameLQ,[rows,cols]);
    
    writeVideo(lqVideo,frameLQ);
end
close(lqVideo);



