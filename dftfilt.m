function g = dftfilt(f,H)

H = fspecial('sobel');

%%
%Step 2: Pad the image
[P,Q] = paddedsize(f);
[m,n] = size(f);

PaddedImage = zeros(P,Q);
PaddedImage(1:m,1:n) = f(1:m,1:n);
PaddedImage = mat2gray(PaddedImage);

%%
% %Step 3: Multiply by (-1)^(x+y)
% for i = 1:m+P
%     
%     for j = 1:n+Q
%         PaddedImage(i,j) = PaddedImage(i,j)*( (-1)^(i+j));
%     end
%     
% end

%%
%Step 4: Discrete Linear Transformation
FourierTransform = fft2(PaddedImage,P,Q);
FourierFilter = fft2(H,P,Q);

FourierTransfrom = fftshift(FourierTransform);
%FourierFilter = fftshift(FourierFilter);


%%
%Step 5: Multiply
Multiplied = FourierFilter.*FourierTransform;
Inverse = ifft2(Multiplied);
g = Inverse(2:size(f,1)+1,2:size(f,2)+1);


%%
%Step 6:
end