%Function to get the similarity of an image

function similarity = CompareFrames(LQImage,HQImage)

% Both images are RGB Colour images
% Extract an 8x8x8 colour histogram from each image
bins = 8;
h1 = getPatchHist(LQImage, bins);
h2 = getPatchHist(HQImage, bins);
% compare their histograms using the Bhattacharyya coefficient
simcolour = compareHists(h1,h2);
% 0 = very low similarity
% 0.9 = good similarity
% 1 = perfect similarity


% Both images are RGB Colour images
% Extract an 8x8x8 colour SPATIOGRAM from each image
bins = 8;
[h1,mu1,sigma1] = getPatchSpatiogram_fast(LQImage, bins);
[h2,mu2,sigma2] = getPatchSpatiogram_fast(HQImage, bins);

% compare their histograms using the Bhattacharyya coefficient
simspatial = compareSpatiograms_new_fast(h1,mu1,sigma1,h2,mu2,sigma2);
% 0 = very low similarity
% 0.9 = good similarity
% 1 = perfect similarity

similarity = (0.35*simspatial + 0.65*simcolour);

end