%This function reorders the images such that they are in sequential order

function imagestream = reorderImages(imagestruct)

    indices = [];

    for i = 1:length(imagestruct)
        
        for j = 1:length(imagestruct)
            
            if(imagestruct(j).framePos == i)
                imagestream(i) = struct('cdata',imagestruct(j).cdata,'framePos',imagestruct(j).framePos);
            end
            
        end
        
    end

    
end