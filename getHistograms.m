
function s = getHistograms(Frames)
    
    for i = 1:numel(Frames)

        % Both images are RGB Colour images
        % Extract an 8x8x8 colour histogram from each image
        bins = 8;
        colourHist = getPatchHist(Frames(i).cdata, bins);

        % Both images are RGB Colour images
        % Extract an 8x8x8 colour SPATIOGRAM from each image
        bins = 8;
        [spatialh,mu,sigma] = getPatchSpatiogram_fast(Frames(i).cdata, bins);

        s(i)= struct('colourHist',colourHist,'spatialh',spatialh,'mu',mu,'sigma',sigma);

    end
end