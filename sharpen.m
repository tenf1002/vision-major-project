%Sharpen Image function

function SharpenedImage = sharpen(InputImage,multiplier,display)

     filter = multiplier*fspecial('sobel');
             if(isstruct(InputImage))

                 for i = 1:numel(InputImage);
                    horedges = imfilter(InputImage(i).cdata,filter,'symmetric');
                    vertedges = imfilter(InputImage(i).cdata,filter','symmetric');
                    edges = horedges + vertedges;
                    InputImage(i).cdata = imsubtract(InputImage(i).cdata, edges);
                 end
                 SharpenedImage = InputImage;
             else
                 horedges = imfilter(InputImage,filter,'symmetric');
                 vertedges = imfilter(InputImage,filter','symmetric');
                 edges = horedges + vertedges;
                 SharpenedImage = imsubtract(InputImage, edges);

                 if(display)
                    figure;
                    subplot(1,2,1);
                    imshow(InputImage);
                    title('Original');
                    subplot(1,2,2);
                    imshow(SharpenedImage);
                    title('Sharpened');
                 end
             end
             
     
end

