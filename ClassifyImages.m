%Classify images

function imagestruct = ClassifyImages(LQFrames)
    
    Threshold = 0.1;
    %Find the colour and spatial histograms of the system
    lqhists = getHistograms(LQFrames);
    classed = 0;
    %Now we go through all of the low quality frames and classify them with
    %relation to the differences in images
    element = 1;
    classcounter = 0;
    frameposition = 1:numel(LQFrames);
    
    %string = strcat('Class',num2str(classcounter));
    %imagestruct(1) = struct('Class',string,'cdata',LQFrames(1).cdata);
    currentImage = LQFrames(1).cdata;
    %LQFrames(1) = [];
    
    
    while(numel(LQFrames)>0)
        
        removeIndices = [];
        for i = 2:numel(LQFrames)
            [r,c,d] = size(LQFrames(i).cdata);
            currentImage = imresize(LQFrames(1).cdata,[r,c]);
            res = 1 - CompareFrames(currentImage,LQFrames(i).cdata);
            res2 = (sum(sum(imsubtract(rgb2gray(currentImage),rgb2gray(LQFrames(i).cdata)))));
            if( (res < Threshold) & (res2 > 30))
                string = strcat('Class',num2str(classcounter));
                imagestruct(element) = struct('Class',string,'cdata',LQFrames(i).cdata,'framePos',frameposition(i));
                element = element+1;
                %frameposition(i) = [];
                %LQFrames(i) = [];
                classed = 1;
                removeIndices = [removeIndices,i];
            end
            
        end
        
        string = strcat('Class',num2str(classcounter));
        imagestruct(element) = struct('Class',string,'cdata',LQFrames(1).cdata,'framePos',frameposition(1));
        element = element+1;
        classcounter = classcounter + 1;
        LQFrames([1,removeIndices]) = [];
        frameposition([removeIndices,1]) = [];
        classed = 0;
        if(numel(LQFrames) > 0)
            currentImage = LQFrames(1).cdata;
        end
    end

end