function result = sharpMetric(image)
    % Check if rgb
   
    
    global goodProp;
    
    [goodProp.rows goodProp.cols goodProp.color] = size(image);
    
    if goodProp.color == 3
        image = rgb2gray(image);
    end
    
    % Increment
    i = 0;
    % Get differences
    for r = 2:(goodProp.rows-1)
        for c = 2:(goodProp.cols-1)
            
                
            % Another loops
            for r2 = (r-1):(r+1)
                for c2 = (c-1):(c+1)
                    if r2~=c2
                        % Increment i
                        i = i + 1;
                        
                        %Delta I
                        deltaISq(i) = (image(r,c)-image(r2,c2))^2;
                        
                    end
                end
            end
        end
    end
    
    % Gx2
    gx2 = sum(deltaISq)/i;
    
    % Get acutance
    result = (gx2/mean(image(:)))*1e4;
end
                        