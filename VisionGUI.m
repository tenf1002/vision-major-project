function varargout = VisionGUI(varargin)
% VISIONGUI MATLAB code for VisionGUI.fig
%      VISIONGUI, by itself, creates a new VISIONGUI or raises the existing
%      singleton*.
%
%      H = VISIONGUI returns the handle to a new VISIONGUI or the handle to
%      the existing singleton*.
%
%      VISIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VISIONGUI.M with the given input arguments.
%
%      VISIONGUI('Property','Value',...) creates a new VISIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before VisionGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to VisionGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help VisionGUI

% Last Modified by GUIDE v2.5 07-Oct-2016 15:49:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @VisionGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @VisionGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end




% End initialization code - DO NOT EDIT


% --- Executes just before VisionGUI is made visible.
function VisionGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to VisionGUI (see VARARGIN)

% Choose default command line output for VisionGUI
handles.output = hObject;

%handles.videofilename = '';
% Update handles structure
guidata(hObject, handles);
%set(hFig,'Tag', 'tformChoiceGui','HandleVisibility','on');
% UIWAIT makes VisionGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = VisionGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in videoBrowseButton.
function videoBrowseButton_Callback(hObject, eventdata, handles)
% hObject    handle to videoBrowseButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles.videofilename,handles.videofilepath] = uigetfile({'*.mp4';'*.mov'},'File Selector');

set(handles.videocheckbox,'Value',1);
guidata(hObject,handles);










% --- Executes on button press in browseTrailerButton.
function browseTrailerButton_Callback(hObject, eventdata, handles)
% hObject    handle to browseTrailerButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles.trailerfilename,handles.trailerfilepath] = uigetfile({'*.mp4';'*.mov'},'File Selector');
set(handles.trailercheckbox,'Value',1);
guidata(hObject,handles);


% --- Executes on button press in trailercheckbox.
function trailercheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to trailercheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of trailercheckbox


% --- Executes on button press in videocheckbox.
function videocheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to videocheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of videocheckbox


% --- Executes on button press in startButton.
function startButton_Callback(hObject, ~, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if( get(handles.trailercheckbox,'Value') == 1 & get(handles.videocheckbox,'Value') == 1)
    set(handles.playButton,'Enable','off');
    set(handles.stateText,'String','Converting....');
    set(handles.stateText,'Visible','on');
    [handles.newVideo,handles.oldvideo] = applytovideo(strcat(handles.videofilepath,handles.videofilename),strcat(handles.videofilepath,handles.trailerfilename));
    set(handles.stateText,'String','Conversion Completed!');
    set(handles.playButton,'Enable','on');
    guidata(hObject,handles);
end

function playButton_Callback(hObject, eventdata, handles)
displaybothvideo(handles.newVideo,handles.oldvideo);
    



function closeButton_Callback(hObject, eventdata, handles)
set(handles.playButton,'Enable','off');
set(handles.stateText,'Visible','off');
set(handles.trailercheckbox,'Value',0);
set(handles.videocheckbox,'Value',0);
set(handles.playButton,'String','Play Video');
handles.trailerfilename = [];
handles.trailerfilepath = [];






