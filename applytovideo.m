function [reformedVideo,oldvideo] = applytovideo(videoname,trailername)



%%

%=======This is the file that we will run stuff in.

%% Constants, variables and functions

getPercentageDifference = @(actual,predict) abs(actual - predict)./actual;
fixingFunctions = {'fixContrast','fixSharpness','fixSpatialEntropy'};
percentageDifference = [.5,.5,5];

%% Get good and bad quality movies

VideoFrames = splitToFrames(videoname);
oldvideo = VideoFrames;
TrailerFrames = splitToFrames(trailername);
reformedVideo = TrailerFrames;

%% Frame Alignment and Environment Removal

% The function uses the bad quality video and  dvaligns it along with removing
% the environment - Mikes
videoAligned = getAlignedMovie(VideoFrames);

%%
%Sharpen
multiplier = findSharpMetricValue(VideoFrames(1).cdata,TrailerFrames(1).cdata);
sharpenedLQFrames = sharpen(VideoFrames,0.35,0);
%displaybothvideo(sharpened,VideoFrames);

%% Get Matched scenes

% The function takes the good quality and bad quality images and returns
% the vectors which correspond to each other. So position 1 of both vectors
% contains the frames at which the matched scene occurs. The third vector,
% as percentage, gives the reliability of the matches.

[goodQualPositions, badQualPositions, matchingError] = getMatchedScenes(goodQualVideo,badQualVideo);



%% Baseline Determination

% Get a good quality image and a bad quality image. Later on, this will be
% put into a loop

goodQualImg = goodQualVideo(goodQualPositions(1));
badQualImg = badQualVideo(badQualPositions(1));


% Get the metrics for the images as a matrix. The function would have
% variable output and one can obtain the metric order. This is to know what
% we do
[goodQualImgMetric,metricOrder] = getMetric(goodQualImg);
badQualImgMetric = getMetric(badQualImg);


%% Align Metrics

metricDifference = getPercentageDifference(goodQualImgMetric,badQualImgMetric);
[~,sortingOrder] = sort(metricDifference);


% Make fixes to error
for n = sortingOrder
    
    % Get the fixing function and error
    currentFixingFunction = str2func(fixingFunctions{n});
    
    % Get current error
    currentError = metricDifference(n);
    
    % Loop until minimised to error
    while currentError > percentageDifference
        
        % Apply fix
        
        % Store settings
        
        % Get new error
        currentError = getMetric(badQualImg,n);
        
    end
    
end


%% Classify the bad quality image in terms of its measured metrics

%This function classifies into a struct the low quality images in terms of
%their colour and spatial histograms. Can change weighting to test for the
%recolouring. It outputs a struct of images including their original order
%so they can be reordered later, and also the class that they belong to.

classifiedLQImages = ClassifyImages(VideoFrames);
[classedHQImages,~,~,~] = ClassifyHQImages(classidiedLQImages,SharpenedLQFrames);

%% Recolor
%Segmentation - Amrit
%Amrit



%%
%Reorder images once they have been split up into classifications and
%recolour

%Ordered = reorderImages(imagestream);

%% Evaluate output


%Todd- Add in mathematical percentage change for sharpness
%[percentchange,difference] = determineSharpnessChange(matchedHQ,matchedLQ,VideoFrames);


end