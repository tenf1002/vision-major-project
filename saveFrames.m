function saveFrames(Frames)

    for i = 1:numel(Frames)
        string = strcat('Frame',num2str(i),'.png');
        imwrite(string,Frames(i).cdata);
    end

end